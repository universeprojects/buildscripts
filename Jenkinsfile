import java.util.regex.Matcher
import java.util.regex.Pattern

node('master') {
    boolean isHotfix = false
    boolean runVoidspaceTests
    boolean runVoidspaceDeploy
    boolean buildVoidspaceInProdMode
    boolean runStartonateTests
    boolean runStartonateDeploy
    boolean runImageDeploy
    boolean skipBinaries
    boolean ignoreCommit
    String buildNumber
    String tag

    stage('Checkout') {
        checkout scm
        buildNumber = "${env.BUILD_NUMBER}"
        String branchName = "${env.BRANCH_NAME}"
        if (branchName) {
            final Pattern hotfixPattern = Pattern.compile("hotfix/build/(\\d+)")
            final Matcher matcher = hotfixPattern.matcher(branchName)
            isHotfix = matcher.matches()
            if (isHotfix) {
                String hotfixBuild = matcher.group(1)
                buildNumber = "$hotfixBuild+${env.BUILD_NUMBER}"
            }
        }
        tag = "build-$buildNumber"
        sh "git log --format=%B -n 1 > .git/commit-message"
        String commitMessage = readFile('.git/commit-message')?.trim()
        runVoidspaceTests = !commitMessage.contains("[skip-tests]") && !params.skipVoidspaceTests
        runVoidspaceDeploy = !isHotfix && !commitMessage.contains("[skip-deploy]") && !params.skipVoidspaceDeploy
        runStartonateTests = !isHotfix && params.runStartonateTests
        runStartonateDeploy = !isHotfix && commitMessage.contains("[deploy-startonate]") || params.runStartonateDeploy
        runImageDeploy = params.runImageDeploy == null ? true : params.runImageDeploy
        buildVoidspaceInProdMode = isHotfix || commitMessage.contains("[prod-compile]") || params.buildVoidspaceInProdMode
        skipBinaries = commitMessage.contains("[skip-binaries]") || params.skipBinaries
        ignoreCommit = commitMessage.contains("[ignore]")
        if(commitMessage.contains("[deploy-startonate-only]")) {
            runImageDeploy = false
            runVoidspaceTests = false
            runVoidspaceDeploy = false
            runStartonateDeploy = true
        }
    }


    if (ignoreCommit) {
        stage('Tag') {
            echo "Ignoring commit"
        }
        stage('Start Jobs') {
            echo "Ignoring commit"
        }
        currentBuild.result = 'SUCCESS'
        return
    }

    stage('Tag') {
        sshagent(['git']) {
            sh "git tag $tag"
            sh "git push origin $tag"
        }
    }

    stage('Start Jobs') {
        final int basePeriod = 5
        int quietCounter = 0
//        if (runImageDeploy) {
//            build job: 'Voidspace-Deploy-Images', parameters: [string(name: 'buildToDeploy', value: "$buildNumber")], wait: false, quietPeriod: (++quietCounter) * basePeriod
//        }
        if (runStartonateDeploy || runStartonateTests) {
            build job: 'Startonate-Build', parameters: [
                string(name: 'buildNumber', value: "$buildNumber"),
                booleanParam(name: 'runTests', value: runStartonateTests),
                booleanParam(name: 'deploy', value: runStartonateDeploy),
            ], wait: false, quietPeriod: (++quietCounter) * basePeriod
        }
        if (runVoidspaceTests || runVoidspaceDeploy) {
            build job: 'Voidspace-Build', parameters: [
                string(name: 'buildNumber', value: "$buildNumber"),
                booleanParam(name: 'runTests', value: runVoidspaceTests),
                string(name: 'deployVersions', value: runVoidspaceDeploy ? "nightly" : ""),
                booleanParam(name: 'prodBuild', value: buildVoidspaceInProdMode),
                booleanParam(name: 'deployBinaries', value: !skipBinaries),
            ], wait: false, quietPeriod: (++quietCounter) * basePeriod
        }
    }
}